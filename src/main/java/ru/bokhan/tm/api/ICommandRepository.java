package ru.bokhan.tm.api;

import ru.bokhan.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArguments(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}
